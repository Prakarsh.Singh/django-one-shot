from django.shortcuts import render
from .models import TodoList


# Create your views here.


def index(request):
    index_todolist = TodoList.objects.all()
    context = {
        "todolist": index_todolist,
    }
    return render(request, "todos/index.html", context)
